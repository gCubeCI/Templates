# Authors

* **Massimiliano Assante** ([ORCID](https://orcid.org/0000-0002-3761-1492)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)

* **Leonardo Candela** ([ORCID](https://orcid.org/0000-0002-7279-2727)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)

* **Giampaolo Coro** ([ORCID](https://orcid.org/0000-0001-7232-191X)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)

* **Luca Frosini** ([ORCID](https://orcid.org/0000-0003-3183-2291)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)
  
* **Francesco Mangiacrapa** ([ORCID](https://orcid.org/0000-0002-6528-664X)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)

* **Alfredo Oliviero** ([ORCID]( https://orcid.org/0009-0007-3191-1025)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)

* **Pasquale Pagano** ([ORCID](https://orcid.org/0000-0001-6611-3209)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)

* **Giancarlo Panichi** ([ORCID](http://orcid.org/0000-0001-8375-6644)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)

* **Biagio Peccerillo** ([ORCID](https://orcid.org/0000-0002-4998-0092)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)
  
* **Marco Procaccini**  ([ORCID](https://orcid.org/0000-0002-9719-2672)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)

## Past contributors

* **Donatella Castelli** ([ORCID](https://orcid.org/)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)

* **Roberto Cirillo** ([ORCID](https://orcid.org/0009-0006-2359-4202)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)

* **Lucio Lelii** ([ORCID](https://orcid.org/0000-0001-7581-5325)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)

* **Valentina Marioli** [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)

* **Ahmed Salah Tawfik Ibrahim** ([ORCID](https://orcid.org/0009-0001-3009-5755)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)

## Other Contributors

* **Mauro Mugnaini** ([Nubisware S.r.l.](https://www.nubisware.com))
  
* **Marco Lettere** ([Nubisware S.r.l.](https://www.nubisware.com))

* **Fabio Simeoni** - FAO of the UN, Italy

* **Andrea Manzi** ([ORCID](https://orcid.org/0000-0001-7949-2199)) - [EGI Foundation](https://www.egi.eu/egi-foundation/)
