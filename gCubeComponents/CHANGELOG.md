This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "project name"

## [Unreleased]

- description [#TICKET_NUMBER]
- description [#TICKET_NUMBER]
- ...

## [v1.1.0] - 2020-01-31

- description [#TICKET_NUMBER]
- description [#TICKET_NUMBER]
- description [#TICKET_NUMBER]
- description [#TICKET_NUMBER]
- description [#TICKET_NUMBER]
- ...

## [v1.0.0] - 2019-10-20

- description [#TICKET_NUMBER]
- description [#TICKET_NUMBER]
- description [#TICKET_NUMBER]
- description [#TICKET_NUMBER]
- ...
