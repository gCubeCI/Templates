# Project Title

One paragraph of project description goes here.

## Structure of the project

* Relevant information about how the repository is organized.
* Description of the Maven modules (if any).
* Any information needed to work with the code.
* Preview for WebApp:

![WebApp Preview](./images/Example.png)

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [JAX-RS](https://github.com/eclipse-ee4j/jaxrs-api) - Java™ API for RESTful Web Services
* [Jersey](https://jersey.github.io/) - JAX-RS runtime
* [Maven](https://maven.apache.org/) - Dependency Management
* ...

## Documentation

Relevant wiki link(s).

## Change log

See [CHANGELOG.md](CHANGELOG.md).

## Authors

* **John Doe** ([ORCID](https://orcid.org/...-...)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)
* **...** - ...

## How to Cite this Software

Tell people how to cite this software.

* Cite an associated paper?
* Use a specific BibTeX entry for the software?

```Latex
    @Manual{,
        title = {.. projec title ..},
        author = {{Infrascience Group}},
        organization = {ISTI - CNR},
        address = {Pisa, Italy},
        year = 2019,
        note = {...},
        url = {http://www.http://gcube-system.org/}
    } 
```

## License

This project is licensed under the license **EUPL V. 1.2** - see the [LICENSE.md](LICENSE.md) file for details.

## About the gCube Framework

This software is part of the [gCubeFramework](https://www.gcube-system.org/gCubeFramework): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.

The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
